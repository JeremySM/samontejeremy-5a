import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyC6XmXUqmSPfSStEt2EMEa3K_ukXzp_sIQ",
  authDomain: "logfirebase-eed60.firebaseapp.com",
  projectId: "logfirebase-eed60",
  storageBucket: "logfirebase-eed60.appspot.com",
  messagingSenderId: "176677728169",
  appId: "1:176677728169:web:a1d0524f9446e608eb9eb8",
  measurementId: "G-73WRLS2CN1"
};

export const app = initializeApp(firebaseConfig);